// document - refers to the whole webpage
// querySelector - used to select a specific object (HTML elements) from the document (webpage)
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// document.getElementById
// document.getElementByClass
// document.getElementByTagName
// const txtFirstName = document.getElementById('txt-first-name');
// const spanFullName = document.getElementById('span-full-name');

// whenever a user interacts with a webpage, this action is considered as an event
// addEventListener - function that takes two arguments
// 'keyup' - string identifying an event
// function that the listener will execute once specified event is triggered

txtFirstName.addEventListener('keyup', (event) => {
	// innerHTML - property sets or return the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	// event.target - contains the element where the event happened
	console.log(event.target);
	console.log(event.target.value);
});